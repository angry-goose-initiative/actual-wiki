This page describes how to create run simulations in order to experiment with and test
LETC at full-chip, or a particular module/sub-block of LETC

# Non-UVM Testbenches

## Install a simulator

In order to simulate LETC you need a SystemVerilog-compatible simulator. At the moment we support the following three options:

- [Verilator](https://verilator.org/guide/latest/install.html)
- xsim from [Vivado](https://www.xilinx.com/support/download.html)
- vsim, part of ModelSim/Questa (by Siemens, but you can likely get a free license if you install [Intel's Quartus Prime Lite](https://www.intel.com/content/www/us/en/products/details/fpga/development-tools/quartus-prime/resource.html) and register at their "[Self Serve Licensing Centre](https://licensing.intel.com/psg/s/)")

Regardless of the simulator you choose, ensure the simulator binary (and relevant helper binaries it uses) are in your PATH.

For viewing waves, you can use the native viewers that are a part of xsim and vsim. Verilator doesn't have a waveviewer of its own, but you can use [GTKWave](https://gtkwave.sourceforge.net/) instead. You can also use GTKWave in place of the viewer for the other simulators if you prefer.

Then proceed onward!

## Running a testbench

`cd verif/nonuvm` to switch to the directory where all of the testbenches live.

You can then do `make help` to see your options. It will look something like this:

```
This is the Makefile for running the non-UVM testbenches!

Usage: make TBENCH=block/subdirs/tbench_folder SIMULATOR={verilator|xsim|vsim} WAVE_VIEWER={gtkwave|native} {compile|simulate|waves|clean}

[...]
```

The only mandatory variable to specify is TBENCH. This is a path to the subdirectory containing the testbench to run, for example `common/fifo/fifo_0r1w`.

SIMULATOR defaults to `verilator` and WAVE_VIEWER to `gtkwave` by default.

Four options are available. `compile` compiles the sub-block or IP into a simulator binary. `simulate` actually runs the simulation. `waves` opens the resulting waveform dump in the wave viewer you specify. And lastly `clean` removes all build artifacts.

Example:

```
$ make TBENCH=common/fifo/fifo_0r1w SIMULATOR=xsim WAVE_VIEWER=gtkwave waves
make[1]: Entering directory './common/fifo/fifo_0r1w'
Compiling testbench: common/fifo/fifo_0r1w
Top module: fifo_0r1w_tb
[...]
Finished compiling testbench: common/fifo/fifo_0r1w
Simulating testbench: common/fifo/fifo_0r1w
[...]
Finished simulating testbench: common/fifo/fifo_0r1w
Waves output: ./common/fifo/fifo_0r1w/build/xsim_waves.vcd
Viewing testbench waves for testbench: common/fifo/fifo_0r1w
[...]
```

And GTKWave will open after the simulation generates the dump.

## Creating a new testbench

If you want to create your own non-UVM testbench (which is really useful in particular for
quickly iterating on a design of a particular block) simply copy an existing folder containing one and modify
the files as appropriate.

In particular, the bare minimum you'll want to copy is the `Makefile` and testbench verilog from an
existing block. Then you can simply modify the `Makefile` with the name of our testbench module, and
a list of all RTL files that need to be compiled in. Lastly modify the testbench to work with your
DUT and BOOM!

That's it! The `verif/nonuvm/Makefile` makefile will automatically work with your setup: all you need
to do is specify the relative path to the folder where your Makefile and testbench are for the TBENCH variable!

# UVM

TODO