This page documents possible relevant timing considerations for LETC and how they may/do influence design decisions.

Discussion may be general or specific to the AMD/Xilinx Zync 7000 series FPGA that LETC is targeting.

# Upper Limits

Useful reference for FPGA limits: https://docs.xilinx.com/v/u/en-US/ds187-XC7Z010-XC7Z020-Data-Sheet

Glancing through this document, since we're running LETC entirely on a single clock domain, it appears that the bottleneck in terms of frequency is the speed of the AXI connection between PS and PL of 250 MHz (-1 speed grade). Other things like the BRAM and other structures can run much faster. So this seems like an ideal target to try to strive for.

# Bulk Memories

LETC requires bulk amounts of state for the following purposes:
- FIFOs for AXI communication and other buffers
- ITLB and DTLB
- L1 data and instruction caches
- Potentially a bulk SRAM for booting, accessible over AXI as a subordinate

## Options for implementation

There are three major forms of memory in the Zync 7000 series FPGA. These include:

- BRAM, a standalone SRAM resource that is the most efficient in terms of area usage but cannot be read combinationally
- Slice Registers, which are in smaller supply but are more flexible for general logic and can be read combinationally
- Slice LUTs, which are usually only programmed once during FPGA configuration to implement combinatorial logic functions, but can actually be used as SRAM as well!

We will make use of all of these in the design. Ideally larger memories in RTL should prefer BRAM if possible, but sadly this may not always be feasible as discussed in the next section.

## Timing Considerations

The most prominent area where we expect to struggle with timing is in LETC Core.

We've attempted to lessen the impact by implementing a 6-stage pipelined design. The additional fetch stage gives us one cycle for ITLB lookup and one for L1I$ access. We have created two execute stages for a similar reason. Since BRAM has one cycle of latency, this allows us to create a functionally correct design that leverages it.

The problem however is that the location of the latency of the memory is at the front of the memory rather than the end as shown:

![simplified_bram_model drawio](https://github.com/angry-goose-initiative/wiki/assets/40674968/6ed605ce-b5fb-4fb0-b1c3-f00fcce1257c)

This is potentially an issue, particularly between the Fetch 2 and Decode stages. Instead of the pipeline stage boundary being between F2 and Decode, the nearest one will instead lie between F1 and F2! This means we will have a long path from the output of the L1I$ address registers, through the BRAM, through Decode's register file, into the flop stage between Decode and E1. This may be problematic...

Although BRAMs can have as low as one cycle of latency if you just use the mandatory address and control signal flops at the front of the memory, AMD recommends also using the optional flops that are at the immediate output of the memory as well. This makes timing closure easier but will also mean we'd have to add more stages to the Core pipeline!

Possible ideas include:
- Just using the optional output flops on the output of the L1I$, not on the ITLB. The input flops to the L1I$ will act in the place of the output flops to the ITLB, still easing its timing closure (though not as much as if we used the ITLB's output flops), and the L1I$ output flops will serve as the boundary between F2 and D. We then mask the latency of the ITLB by setting the address and the PC "in parallel" assuming this doesn't cause any functional problems.
- If the above idea does cause functional problems, switch out the ITLB to use Slice LUTs as memory instead of using BRAM. This allows us combinational reads and should resolve the problem.
- Add another tier to the cache hierarchy with Slice LUTs as memory, backed by the larger BRAMs.
- Alternatively, redesign the pipeline with more stages.

Regardless of what we choose, we may also need to implement something similar for data accesses.

## Decision

Not yet decided...

# Resets

Although I'm used to async resets, in LETC synchronous resets should be preferred as AMD claims they are more efficient. Certain blocks like the DSPs (which we may use to help implement the M extension) also only synchronous so failure to use such reset types may result in them failing to be inferred.

# DSP/Multipliers

In testing, while we could barely scrape by at 250 MHz with only 4 DSP multiplier pipeline stages to implement RISC-V multiplication instructions, given this will be sitting outside of the main LETC Core pipeline anyways, it makes sense to use all 5 stages (one input latency stage and 4 for the multiplication).

# TODO others