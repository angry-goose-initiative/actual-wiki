# General C/C++
- Try to keep lines <= 100 characters (comments must always follow this unless there's a really good reason not to)
- Prefer `#pragma once` over `#ifndef` header guards for maintainability (yes, we know this isn't standard)
- Files should be `snake_case`

# C++ Specific
## Naming
- Classes, structs, etc should be `PascalCase`
- Constants should be `ALL_CAPS`
- Functions/methods, namespaces, and variables should be `snake_case`
## General
- Use `constexpr` instead of `#define` for constants
- Use `consteval` when possible
- Use scoped enums
- Avoid macros
- Consider using `std::optional` and `std::variant`

# Assembly
- Use .S instead of .s for assembly files

# Rust

todo

# SystemVerilog

todo