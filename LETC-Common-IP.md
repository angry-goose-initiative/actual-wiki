This page documents generally-project-agnostic IP developed for use with LETC.

These modules are highly generic so you're welcome to use them in your own project! We'll likely do the same for future projects :)

# FIFOs

Fifo variants are available under `rtl/common/fifo/fifo_XrYw.sv`, where X and Y are the latencies in clock cycles for reads and writes respectively. These are tuned to map well to Zync 7000 memory resources during synthesis, but note that choosing variants with lower latencies may impede this (and also may introduce timing challenges due to potential bypassing around state elements).

# todo others