# Register RAW Hazards

Note that we ignore hazards associated with `x0` because that register is always 0, and thus the hazard is not actually present.

Also note we ignore CSR reads->GPR registers, since we will basically do CSR instructions on their own with all other stages flushed (because CSR accesses can have some really weird side effects, and are so infrequent that this cost doesn't matter).

## Decode

Since we begin accessing registers in the decode stage, this is the stage where we need to begin worrying about hazards.

In addition to wanting to have our register operands prepped for execute 1 to consume, since we decide branches in decode as well, this further complicates things and we need to be careful with stalling or bypassing. However, we do have the benefit of the register file being designed to pass through data when it is written and read on the same clock cycle, so we never need to forward from the writeback stage explicitly.

There are two cases to consider: when decode is processing a control transfer instruction, and when it processes any other instruction.

### Control transfer instructions

When decode is processing a control transfer instruction, it is possible that it will need to consume the contents of `rs1`, `rs2`, or both. This is both when we are processing a conditional branch which must compare the contents of `rs1` and `rs2`, and when executing the `jalr` instruction.

In this case, we don't have the option of waiting until a later stage the perform the bypass since the only connections back to `f1` are from decode. Therefore, if `e1`'s ALU result is targeting a register that is one of the same ones our branch depends on, we must stall, as it would be too long of a combinational path to go from the output flop stage of decode, through `e1`'s ALU, through the branch comparator, into `f1`. We can then bypass the ALU result from the output flop stage of `e1` into decode.

For a similar reason, however, this bypass path does NOT suffice if the result of a memory read in the `e2` stage is depended on in decode. To more easily achieve timing closure, we must also stall in this case as well. This will cause the result to move into the `w` stage where, as discussed before, there is no longer a hazard as the new data can be bypassed through the RF. (The same goes for if the hazard is from the ALU result in the writeback stage)

### Other instructions

There's no hard need to bypass the registers for any other instructions because their values aren't actually used until `e1`. We thus just wait one cycle and deal with the bypassing in that stage instead! This simplifies things because it means the only thing we ever need to bypass to decode is the ALU result after the `e1`->`e2` flop stage!

## Execute 1

We need to worry about register operands in this stage as well. The yet-to-be-written result of a previous computation for either `rs1` or `rs2` may be within `e2` (the output of the `e1` flop stage) or currently in writeback being written to the RF. Unlike `decode`, since we don't have direct RF access, we can't simply rely on getting the "new value" through the RF, we must be explicitly bypassed it from write back. No stalling is necessary (not due to register dependencies at least, we may need to stall for the dtlb or backpressure from `e2`), and bypassing should handle this in entirety! :)

FIXME no actually, we need one cycle of stalling (load to use latency).

## Execute 2 and writeback

In execute 2 there are (effectively) no longer any registers to process. In particular, we've latched the alu result at this point so even if there _was_ a hazard that would have change the ALU's result / target memory address to access it's too late to fix it here anyways. We should have dealt with it in `e1`! Yes, technically we do have `rs2` as the data to store for store instructions, so we could bypass that here instead of stalling in `e1` in the immediate past cycle for it to be ready _specifically just for STOREs_ (and not for `rs1`, only `rs2`), but that minor optimization doesn't seem worth it.

For writeback, well, there are no stages after writeback. So no hazards are possible and there's nothing to bypass from or stall for!

# CSR Hazards

todo

# Instruction RAW Hazards

TODO discuss how we handle `fence.i`

# Useful References

- [My old hazard code for JZJPipelinedCoreC](https://git.jekel.ca/JZJ/jzjpipelinedcorec/src/branch/master/src/jzjpcc/jzjpcc_hazard_unit.sv)