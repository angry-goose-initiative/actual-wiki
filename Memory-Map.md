_This page documents the physical memory map of our implementations_

# IRVE

| Region | Start (inclusive) | End (inclusive) | Size |
| - | - | - | - |
| RAM (Intended for M-Mode) | 0x00000000 | 0x03FFFFFF | 64 MiB |
| RAM (Intended for S-Mode) | 0x80000000 | 0x83FFFFFF | 64 MiB |
| ACLINT | 0xF0000000 | 0xF000BFFF | 48 KiB |
| 16550 UART | 0xF1000000 | 0xF1000007 | 8 B |
| IRVE Debug Address (Output) | 0xFFFFFFFF | 0xFFFFFFFF | 1 B |

Note that the physical address space is actually 34 bits, but an address outside the 32-bit range can only be accessed using virtual memory. We won't place anything at any of these addresses.

IMPORTANT: For an unknown reason (possibly a kernel bug?) the S-Mode Linux kernel does NOT like to be loaded in physical memory at `0xC0000000`. This caused me (JZJ) many hours of debugging pain over several months. No idea why this is the case; does the kernel not like being mapped physically at the same offset that it maps itself virtually? Maybe the `0` offset between the two gets interpreted as some sort of failure value in the kernel? Switching things to be at `0x80000000` fixes the issue. Also the kernel was compiled with QEMU quirks support; perhaps that helped?

# LETC

| Region | Start (inclusive) | End (inclusive) | Size |
| - | - | - | - |
| TBD | TBD | TBD | TBD |