Term | Definition
-----|-----------
AGI | Angry Goose Initiative; the hobbyist project with the goal of learning how to create Linux-capable RISC-V hardware from scratch!
IRVE | Inextensible RISC-V Emulator; A S-Mode Linux kernel capable emulator serving as a reference model for LETC development.
LETC | The Little Engine That Could; A S-Mode Linux kernel capable RISC-V hardware implementation
LETC Core | Major LETC sub-block containing primary CPU datapath, TLBs, L1 caches, MMUs/page table walkers, CSRs, hazard resolution logic, and an interrupt controller.
LETC Matrix | Primary soft-logic AXI interconnect for LETC.

todo more