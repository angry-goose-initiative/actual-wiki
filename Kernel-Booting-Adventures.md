Vdso is a pain… modify the kernel to not call missing symbols related to it for now

# BBL
- Clone riscv-pk
- ../configure --prefix=/opt/riscv --host=riscv32-unknown-elf --with-arch=rv32ima_zicsr_zifencei
- riscv32-unknown-elf-objcopy --change-addresses -0x80000000 ./bbl bbl.out if you want to disassemble to look at this
- Bbl.bin would have already been created expecting to be loaded at 0x00000000 (vector table/reset even laid out in the same way we did by sheer luck!)
- Do `riscv32-unknown-elf-objcopy -O verilog --verilog-data-width=4  --change-addresses -0x80000000 ./bbl bbl.hex` to actually get a hex file

# More Kernel Booting Adventures
Assuming a RISC-V SBI-compatible piece of firmware…
- Load the kernel into memory at the address it expects
    - This should be configurable in the kernel config
    - Perhaps use objcopy to change its position in memory will work if needed (untested)?
- Load the device tree blob (compiled device tree) into memory
    - I’m not sure how much the location matters (there may be some restrictions)
- Register a0 should be loaded with the hart ID, and a1 should be loaded with the address of the DTB in memory
- Configure CSRs and MRET into the RISC-V Linux Kernel’s header (the first two words are executable instructions that actually start the kernel