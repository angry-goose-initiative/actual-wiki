# Overview
Conductor is a project to help/control LETC and consists of two parts:
- A CLI tool that runs on a personal computer
- Software running on the Cora Z7 ARM core receives commands from the CLI and performs the specified operation. This software also performs any initialization required for LETC.
## Command ideas
- Reset and pause execution
- Pause execution
- Step instruction (This may be difficult RTL-wise)
- Read from a memory location
- Write a memory location
- Load a binary/kernel from the host/the sd card
- Read from the register file (This may be difficult RTL-wise)
- Read the pc (This may be difficult RTL-wise)
- Read the current instruction (This may be difficult RTL-wise)
- Potentially option to reprogram the PL with a new bitstream on the fly? So we don't have to reload conductor or anything, enabling faster iteration/testing

## Conductor UART Protocol
### Command Steps
- Conductor CLI sends full command
- Conductor ARM sends ACK and any needed data or sends NACK
### Command structure
- Start of transmission byte
- Command byte
- Any command specific data
- End of transmission byte

### Response structure
- Start of transmission byte
- Echo command byte
- Ack/Nack byte
- If Nack, the Nack reason byte
- If Ack, any command specific data
- End of transmission byte

### Commands
#### SYNC

