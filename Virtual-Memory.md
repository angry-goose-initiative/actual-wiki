_This page describes how virtual memory works on our EEI_

# Overview
- We use SV32 which is the only paging option for a 32-bit system
- Pages are 4 KiB
- Superpages are 4 MiB
- A virtual address (va) is partitioned as follows:

    | 31:22 | 21:12 | 11:0 |
    | :-: | :-: | :-: |
    | VPN[1] | VPN[0] | Page Offset |
    - VPN: Virtual Page Number
- Two-level page table
- 20-bit VPN is translated into a 22-bit physical page number (PPN) and concatenated with the page offset to get a 34-bit physical address
- The page table is 4 KiB (same size as a page and must be aligned to a page boundary)
- When operating in bare mode, 32-bit addresses are zero-extended resulting in a 34-bit physical address

## Page Table Entry

A page table entry (pte) is partitioned as follows:

| 31:20 | 19:10 | 9:8 | 7 | 6 | 5 | 4 | 3 | 2 | 1 | 0 |
| :-: | :-: | :-: | :-: | :-: | :-: | :-: | :-: | :-: | :-: | :-: |
| PPN[1] | PPN[0] | RSW | D | A | G | U | X | W | R | V |

- PPN: Physical page number
- RSW: Reserved for software so this is ignored
- D: Dirty bit, indicates the virtual page has been written since the last time the D bit was cleared
- A: Accessed bit, indicates the virtual page has been read, written, or fetched from since the last time the A bit was cleared
- G: Designates global mapping (mappings that exist in all address spaces). For non-leaf PTEs, the global setting implies that all mappings in the subsequent levels of the page table are global
- U: Indicates if the page is accessible in U-mode
    - If sstatus.SUM is set, S-mode can also access pages with U=1. Fault otherwise
    - Supervisor cannot execute code on pages with U=1
- X: Indicates if page is executable. Attempting to fetch an instruction from a page where this is not set raises a page-fault exception.
- W: Indicates if page is writable. Attempting to execute a store, store-conditional, or AMO instruction whose effective address lies within a page without write permissions raises a store page-fault exception (AMO instruction only raise store page-fault exceptions)
- R: Indicates if page is readable. Attempting to execute a load or load-reserved instruction whose effective address lies within a page without read permissions raises a load page-fault exception
- V: Indicates if PTE is valid
- When X, W, and R are all 0, the PTE is a pointer to the next level of the page table, otherwise it’s a leaf PTE
- Writable pages must be marked as readable (though this might not affect how we implement anything)
- Scheme for managing the A and D bits: When a virtual page is accessed and the A bit is clear, or is written and the D bit is clear, a page-fault exception is raised.





- A PTE address is partitioned as follows:

    | 33:12 | 11:2 | 1:0 |
    | :-: | :-: | :-: |
    | PPN | va.VPN[i] | 0b00 |

# Translation Process

<u>Step 1</u>

Let _a_ = [satp](https://github.com/angry-goose-initiative/wiki/wiki/Control-Status-Registers-(CSRs)#satp).PPN * 2<sup>12</sup>, and _i_ = 1. (satp must be active i.e. the system is in S-mode or U-mode)

<u>Step 2</u>

Let _pte_ be the value of the PTE at address _a_ + (va.vpn[i] * 4). If accessing the PTE violates a PMA (physical memory attribute) or PMP (physical memory protection) check, raise an access-fault exception corresponding to the original access type.

<u>Step 3</u>

If pte.V = 0 (not a valid PTE) or if pte.R = 0 and pte.W = 1 (violates writable pages must also be readable), stop and raise a page-fault exception corresponding to the original access type.

<u>Step 4</u>

Here, we know that the PTE is valid. If pte.R = 1 or pte.X = 1, go to step 5. Otherwise, this PTE is a pointer to the next level of the page table. Decrement i. If i < 0, stop and raise a page-fault exception corresponding to the original access type. Otherwise, let a be pte.PPN * 2^12 and go to **step 2**.

<u>Step 5</u>

At this step, a leaf PTE has been found. Determine if the requested memory access is allowed by the pte.r, pte.w, pte.x, and pte.u bits, given the current privilege mode and the value of the SUM and MXR fields of the mstatus register. If not, stop and raise a page-fault exception corresponding to the original access type.

<u>Step 6</u>

If _i_ = 1 and pte.PPN[0] != 0, this is a misaligned superpage; stop and raise a page-fault exception corresponding to the original access type.

<u>Step 7</u>

If pte.a = 0, or if the original memory access is a store and pte.d = 0, either raise a page-fault exception corresponding to the original access type.

<u>Step 8</u>

The translation is successful. The physical address (pa) is constructed as follows:
- pa.pgoff = va.pgoff
- If _i_ = 1, this is a superpage translation and pa.PPN[0] = va.PPN[0] and pa.PPN[1] = pte.PPN[1]
- Else, pa.PPN[1:0] = pte.PPN[1:0]