This page details the LETC/Conductor boot flow.

# Description

Shortly after the board powers on, its 50MHz crystal oscillator starts up. Soon after the Ethernet PHY's PLL (on the board) locks onto this clock and generates a 125MHz clock output, which is connected to a PL clock pin. This clock is not software controllable and thus is only of use for debugging purposes.

Additionally the 50MHz clock is fed directly into the PS clock input and, once a computer is attached, it can program the OCM with Conductor ARM.

Conductor ARM begins and performs basic init, in particular setting up UART 0. It then waits for Conductor CLI to handshake and attach to it. Shortly after the Conductor CLI will boot and also look for Conductor ARM. The two will handshake and PL and main peripheral configuration will begin.

Conductor CLI is mostly idle during this time, save from servicing Conductor ARM requests and potentially prompting the user for files to load. On the other hand, Conductor ARM is busy at work configuring additional peripherals, the DRAM controller and PHY, and programming the PL with a bitstream. Once the DRAM subsystem is initialized it programs DRAM with the appropriate memory contents for LETC to use.

Two important things to note at this point:
- After programming DRAM, ARM L1 and L2 caches should be flushed. Given that LETC itself will use the L2 and the whole subsystem should be cache coherent, this shouldn't matter, but better safe than sorry.
- Interfaces between the PS and PL **SHOULD NOT YET BE CONFIGURED**. This will be explained in a bit...

Now comes the tricky bit: managing LETC clocks and resets, and configuring PS-PL interfaces. **The order of operations here is VITALLY IMPORTANT to avoid issues**.

To start, Conductor ARM should configure the clock controller to enable `FCLK_CLK0` and set `FCLK_RESET0_N` to 0. It is not vitally critical which of these occurs first. However, once the clock is running at 250 MHz and `FCLK_RESET0_N` is 0, **Conductor ARM must wait, at minimum, 4 `FCLK_CLK0` clock cycles before beginning to configure PS-PL interfaces.** It is recommended that it wait significantly more than this (more time doesn't hurt).

Why is this? Until LETC is reset, the PL **should not be able to interact with any part of the PS whatsoever**. This includes both before and after the PL is configured, as during the time before reset LETC's registers may have powered up in an unknown state. LETC may therefore behave erratically, potentially clobbering DRAM and writing nonsense to Conductor ARM's memory and PS peripherals.

Conductor ARM must wait a bare-minimum of 4 cycles specifically since, according to AMD's Zync 7000 TRM, `FCLK_RESET0_N` ought to be treated as an asynchronous signal, even relative to `FCLK_CLK0`. [LETC's standard CDC synchronizer](https://github.com/angry-goose-initiative/letc/blob/main/rtl/common/cdc/cdc_synchronizer.sv) has 3 cycles of latency for safety, and then once this synchronized reset is asserted, since LETC's resets are synchronous, they require one additional cycle to take effect.

Once PS-PL interfaces are configured, it is safe to now set `FCLK_RESET0_N` to 1. Shortly after this LETC should begin executing instructions!

# Timing Diagram

![wavedrom](https://github.com/angry-goose-initiative/wiki/assets/40674968/db4aa0fe-e968-4ed0-9716-278f4383c5fc)

## Software State Legend

### Conductor CLI

|Letter|State Description|
|-|-|
|A|Wait for CLI and handshake|
|B|Wait for ARM to perform init, printing status as we go. May require input from user to specify files to load for example.|
|C|"Running mode" state with relevant commands now accepted.|

### Conductor ARM

|Letter|State Description|
|-|-|
|A|Wait for CLI and handshake|
|B|Configure NON-PS-PL peripherals and controllers, and the PL itself. Also program DRAM.|
|C|Start PL clock and assert PL reset.|
|D|Configure PS-PL interfaces, including AXI|
|E|De-assert PL reset|
|F|"Running mode"|