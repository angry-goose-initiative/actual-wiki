*This page documents how to compile and run the Linux kernel on IRVE and LETC.*

# Common initial steps

## Installing a toolchain

To compile the Linux kernel for a RISC-V platform, you need to install a toolchain.

We use https://github.com/riscv-collab/riscv-gnu-toolchain. Unlike with RVSW software,
for the kernel we can't use the Newlib version of the toolchain, otherwise you will get
dynamic linking errors pertaining to "vDSO" (used to speed up certain system calls).

However, **you must have both toolchains installed simultaneously** so you can compile
both compile the kernel/userspace programs and bare metal software!

So first follow the instructions [here](https://github.com/angry-goose-initiative/wiki/wiki/Setting-up-the-RISC%E2%80%90V-Toolchain-(Bare-Metal)) to install the Newlib toolchain, and then the ones below to install the glibc one!

1. Do `git clone https://github.com/riscv-collab/riscv-gnu-toolchain.git`

2. `cd` into the directory that was cloned

3. Do `./configure --prefix=/path/to/non/conflicting/prefix/dir --with-arch=rv32ima --with-abi=ilp32`

4. Do `make linux -j <number of threads here>` to actually compile and install the toolchain (not just `make` as that will give you the Newlib version).

5. Ensure all of the `riscv32-unknown-linux-gnu-` tools, contained within the `bin` subdirectory of your prefix, are in your `PATH`.
The `riscv32-unknown-elf-` tools are the Newlib variants, and must be in your `PATH` as well!

With that we are ready to proceed!

## Automated Compilation (Recommended)

The repo https://github.com/angry-goose-initiative/build-linux contains helper thingamabobs to
aid in compiling the Linux kernel.

1. Do `git clone https://github.com/angry-goose-initiative/build-linux.git`

2. `cd` into the directory that was cloned

You're now set to automatically build either the S-Mode/MMU or M-Mode/NOMMU kernels for either IRVE or LETC!

## This is a pain, do you guys have a precompiled kernel I could just use?

Sure! Skip all of this second-toolchain-automated-compilation nonsense by downloading the kernel image file attached to our [IRVE 1.0.0 release](https://github.com/angry-goose-initiative/irve/releases/tag/v1.0.0)

# IRVE

## Automated Compilation

### S-Mode Kernel

Put any additional files you want to access in the booted environment under `initramfs/contents/`.

You'll see that there is already a directory structure there that looks similar to a regular Linux file system.

After doing this and following the common steps, do `make kernel/irve-mmu/arch/riscv/boot/Image`. Add `-j <# of threads>` to speed up the process!

This will fetch the required git modules, and also build `busybox` if it hasn't been built already, which provides command line tools.

It will also build IRVE again in inception mode so you can run IRVE in IRVE! :)

Then the kernel will be compiled, with initramfs contents taken from `initramfs/contents/`!

You'll likely be interested in
`build-linux/kernel/irve-mmu/arch/riscv/boot/Image` and
`build-linux/kernel/irve-mmu/vmlinux`

### M-Mode Kernel

After following the common steps, do `make kernel/irve-nommu/arch/riscv/boot/Image`. Add `-j <# of threads>` to speed up the process!

After a bit of waiting the results will be in `build-linux/kernel/irve-nommu`. You'll likely be interested in
`build-linux/kernel/irve-nommu/arch/riscv/boot/Image` and
`build-linux/kernel/irve-nommu/vmlinux`

Note that unlike the S-Mode kernel, at the moment no initramfs is present, so this isn't that useful except to watch the kernel boot (and then panic due to there not being a root filesystem).

## Running the kernel

We have scripts in the `irve` repo to simplify this!

**IMPORTANT: You must compile IRVE in Release mode with `cmake -DCMAKE_BUILD_TYPE=Release ..` instead of just `cmake ..`.** Things will work in debug mode, however it will take minutes for the kernel to boot and there will be constant verbose debug messages.

Assuming you've already [compiled IRVE with RVSW software](https://github.com/angry-goose-initiative/wiki/wiki/Quickstart:-Running-Hello-World-on-IRVE) (this means you will need the Newlib RISC-V toolchain as well), from the root of that repo do one of the following:

### S-Mode

1. `./scripts/irve_smode_linux.sh path/to/linux/arch/riscv/boot/Image`

With any luck you should see the `OGSBI` RVSW bootloader and SBI implementation run, followed by kernel messages!

You then should be dropped into a shell prompt where you can begin to mess around with things!

To step through the kernel with GDB, do

1. `./scripts/irvegdb_smode_linux.sh path/to/linux/arch/riscv/boot/Image path/to/linux/vmlinux`

The raw binary `Image` is loaded by IRVE, and the symbol-containing `vmlinux` provides debugging symbols to GDB. Execution should pause at 0x80000000, the entry point of the kernel (before it enables paging).

### M-Mode

1. `./scripts/irve_mmode_linux.sh path/to/linux/arch/riscv/boot/Image`

With any luck you should see the `nommulinux` RVSW bootloader run, followed by kernel messages!

To step through the kernel with GDB, do

1. `./scripts/irvegdb_mmode_linux.sh path/to/linux/arch/riscv/boot/Image path/to/linux/vmlinux`

The raw binary `Image` is loaded by IRVE, and the symbol-containing `vmlinux` provides debugging symbols to GDB. Execution should pause at 0x80000000, the entry point of the kernel.

# LETC

## M-Mode Kernel

TODO

## S-Mode Kernel

TODO

# Manual Compilation (Optional)

1. Do `git clone https://github.com/angry-goose-initiative/linux`. This is our modified fork of the Linux kernel used by AGI projects.

2. Do `git switch <branch name>` to switch to the version of our kernel you are interested in. This is likely one of `irve-mmu`, `irve-nommu`, `letc-mmu`, or `letc-nommu`.

3. Choose a Linux config file from the `build-linux` repo under `build-linux/kernel/config/` and copy it to `.config` in the root of the `linux` repo.

4. Optionally tweak the config yourself with `make ARCH=riscv CROSS_COMPILE=riscv32-unknown-linux-gnu- xconfig` to use the graphical Qt interface (what I prefer). You can substitute this with `menuconfig` or other config options if you prefer/don't have Qt libraries installed.

5. To actually cross-compile the kernel, do `make ARCH=riscv CROSS_COMPILE=riscv32-unknown-linux-gnu- all -j <number of threads here>`