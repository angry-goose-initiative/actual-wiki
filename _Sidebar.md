# Contents
## General
- [Home](https://github.com/angry-goose-initiative/wiki/wiki)
- [Code Guidelines](https://github.com/angry-goose-initiative/wiki/wiki/Code-Guidelines)
- [Useful Links](https://github.com/angry-goose-initiative/wiki/wiki/Useful-Links)
- [Directory Structure](https://github.com/angry-goose-initiative/wiki/wiki/Directory-Structure)
- [Terminology](https://github.com/angry-goose-initiative/wiki/wiki/Terminology)

## Guides
- [Quickstart: Running Hello World on IRVE](https://github.com/angry-goose-initiative/wiki/wiki/Quickstart:-Running-Hello-World-on-IRVE)
- [Setting up the RISC‐V Toolchain](https://github.com/angry-goose-initiative/wiki/wiki/Setting-up-the-RISC%E2%80%90V-Toolchain-(Bare-Metal))
- [Setting up LETC in Vivado & Vitis](https://github.com/angry-goose-initiative/wiki/wiki/Setting-up-LETC-in-Vivado-&-Vitis)
- [Simulating LETC](https://github.com/angry-goose-initiative/wiki/wiki/Simulating-LETC)
- [Synthesizing LETC](https://github.com/angry-goose-initiative/wiki/wiki/Synthesizing-LETC)
- [Linux shenanigans](https://github.com/angry-goose-initiative/wiki/wiki/Linux-shenanigans)
- [Creating a Test Program](https://github.com/angry-goose-initiative/wiki/wiki/Creating-a-Test-Program)
- [Using GDB with IRVE](https://github.com/angry-goose-initiative/wiki/wiki/Using-GDB-with-IRVE)
- [Cross Compiling Manually](https://github.com/angry-goose-initiative/wiki/wiki/Cross-Compiling-Manually)

## IRVE/LETC Software Interface (EEI)
- [General EEI Info](https://github.com/angry-goose-initiative/wiki/wiki/General-EEI-Info)
- [Exceptions and Interrupts](https://github.com/angry-goose-initiative/wiki/wiki/Exceptions-and-Interrupts)
- [Memory Map](https://github.com/angry-goose-initiative/wiki/wiki/Memory-Map)
- [Virtual Memory](https://github.com/angry-goose-initiative/wiki/wiki/Virtual-Memory)
- [PMA & PMP](https://github.com/angry-goose-initiative/wiki/wiki/PMA-&-PMP)
- [Control Status Registers (CSRs)](https://github.com/angry-goose-initiative/wiki/wiki/Control-Status-Registers-(CSRs))

## IRVE Implementation

## LETC (Micro)architecture
- [LETC Architecture Overview](https://github.com/angry-goose-initiative/wiki/wiki/LETC-Architecture-Overview)
- [LETC Core](https://github.com/angry-goose-initiative/wiki/wiki/LETC-Core)
- [LETC Core Hazards](https://github.com/angry-goose-initiative/wiki/wiki/LETC-Core-Hazards)
- [LETC Matrix](https://github.com/angry-goose-initiative/wiki/wiki/LETC-Matrix)
- [LETC Bus Protocols](https://github.com/angry-goose-initiative/wiki/wiki/LETC-Bus-Protocols)
- [LETC Common IP](https://github.com/angry-goose-initiative/wiki/wiki/LETC-Common-IP)
- [LETC Timing Considerations](https://github.com/angry-goose-initiative/wiki/wiki/LETC-Timing-Considerations)
- [LETC Memory Brainstorming](https://github.com/angry-goose-initiative/wiki/wiki/LETC-Memory-Brainstorming)

## Notes/Brainstorming
- [LETC/Conductor Boot Flow](https://github.com/angry-goose-initiative/wiki/wiki/LETC-Conductor-Boot-Flow)
- [Conductor Planning](https://github.com/angry-goose-initiative/wiki/wiki/Conductor-Planning)
- [Timers & Counters](https://github.com/angry-goose-initiative/wiki/wiki/Timers-&-Counters)