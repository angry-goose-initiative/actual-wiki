# Prerequisites

Since we are targeting a Zynq 7000 series FPGA, in order to run synthesis for it you need to install [Vivado](https://www.xilinx.com/support/download.html).

Make sure that the `vivado` binary is in your PATH as well so that scripts function correctly.

# Out of Context (Subblock) Synthesis

Out of Context synthesis is where we only synthesize a sub-block of LETC for testing purposes. This allows us to get rough timing, area, and power estimates on sub-blocks of the design and enables faster iteration during development that synthesizing the entirety of LETC.

## Running

`cd synth/subblock` to switch to the directory where the OOC synthesis enviornment lives
You can then do `make help` to see your options. It will look something like this:

```
This is the Makefile for running out of context synthesis!

Usage: make BLOCK=block/subdirs/synth_folder synth

[...]
```

`BLOCK` is a path to the subdirectory containing the subblock to synthesize, for example `common/fifo/fifo_0r1w`.

Example: `make synth BLOCK=common/fifo/fifo_0r1w`

## Setting up for a new block

If you want to set things up for a new block, simply copy an existing folder containing one and modify
the files as appropriate.

In particular, the bare minimum you'll want to copy is the `synthesis.tcl` script. You can then modify it with
the name of the top module of the design, a list of all RTL files that need to be compiled in, and the constraints.

In speaking of constraints, you can either point Vivado towards the `usual.xdc` constraints file in `synth/subblock` or write your own if you need to customize something.

That's it! The `synth/subblock/Makefile` makefile will automatically work with your setup: all you need
to do is specify the relative path to the folder where your files are for the BLOCK variable!

# Full Chip Synthesis

todo