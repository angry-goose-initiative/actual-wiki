_This page contains useful links to external resources_

TODO group links into more categories

# RISC-V Documentation
- [RISC-V Specs](https://riscv.org/technical/specifications/)
- [RISC-V SBI Spec](https://github.com/riscv-non-isa/riscv-sbi-doc)
- [Building up a RISC-V Linux with Buildroot](https://jborza.com/emulation/2020/04/09/riscv-environment.html)
- [Presentation about OpenSBI](https://riscv.org/wp-content/uploads/2019/06/13.30-RISCV_OpenSBI_Deep_Dive_v5.pdf)
- [OpenSBI Repository](https://github.com/riscv-software-src/opensbi)
- [Linux on RISC-V Presentation](https://elinux.org/images/c/cc/Linux_on_RISC-V_%28ELC-E_2022%29.pdf)
    - IMPORTANT: This specifies that M-mode is responsible for generating timer interrupts for S-mode since it doesn’t have access to the registers
- [RISC-V Compatibility Framework](https://riscof.readthedocs.io/en/stable/intro.html)

# FPGA
- [Zync 7000 TRM](https://docs.xilinx.com/r/en-US/ug585-zynq-7000-SoC-TRM)
- [Cora Z7 Reference Manual](https://digilent.com/reference/programmable-logic/cora-z7/reference-manual)
- [FPGA Data Sheet](https://docs.xilinx.com/v/u/en-US/ds187-XC7Z010-XC7Z020-Data-Sheet)
- [MIO/EMIO/Dedicated PS IO/Dedicated PL IO](https://docs.xilinx.com/r/en-US/ug1085-zynq-ultrascale-trm/Signals-Interfaces-and-Pins)

# General Hardware
- [16550 UART - Wikipedia](https://en.wikipedia.org/wiki/16550_UART)
- [AXI Spec](https://developer.arm.com/documentation/ihi0022/hc/)

# Linux
- <https://buildroot.org>
- <https://github.com/torvalds/linux/blob/64569520920a3ca5d456ddd9f4f95fc6ea9b8b45/arch/riscv/kernel/head.S>
- <https://agnimeele.wordpress.com/2019/03/08/compile-linux-from-linus-git-repo-for-riscv-architecture> USEFUL FOR SHOWING HOW TO PLACE THE KERNEL AT A PARTICULAR ADDRESS IN MEMORY BY MODIFYING ITS LINKER SCRIPT

# General Embedded Development

todo

# Legacy Information

- [Old Google Doc we used to use for planning](https://docs.google.com/document/d/1HeZ4l5OxooQuh2kdJ1o-uWJXWzGPsVntSPlf_ByFmWA/edit?usp=sharing)

# To Organize
- <https://gts3.org/2017/cross-kernel.html>
- <https://www.sifive.com/blog/all-aboard-part-6-booting-a-risc-v-linux-kernel>
- <https://www.kernel.org/doc/Documentation/arm64/booting.txt>
- <https://www.sifive.com/blog/all-aboard-part-7-entering-and-exiting-the-linux-kernel-on-risc-v>
- <https://www.sifive.com/blog/all-aboard-part-3-linker-relaxation-in-riscv-toolchain>
- <https://www.sifive.com/blog/all-aboard-part-2-relocations>
- <https://blog.thea.codes/the-most-thoroughly-commented-linker-script>
- <https://danielmangum.com/posts/risc-v-bytes-timer-interrupts>
- <https://blog.pimaker.at/texts/rvc1>
- <https://github.com/riscv-software-src/riscv-tests>
- <https://github.com/takahirox/riscv-rust> This says that BBL is legacy (OpenSBI is not)
- <https://github.com/pimaker/rvc>
- <https://gcc.gnu.org/onlinedocs/gccint/Initialization.html>

- USEFUL patch info about the clint/aclint being for M-mode and SBI for S-mode: https://patchwork.kernel.org/project/linux-riscv/cover/20211007123632.697666-1-anup.patel@wdc.com/
- Describes differences btw clint, plic, clic, etc: https://raw.githubusercontent.com/riscv/riscv-fast-interrupt/master/clic.pdf
