todo introduction

F1/2, E1/2 TLB/cache brainstorming:

Vivado SRAM has address registers at input, no registers at output. Synth tool recommends that there are output flops as well (making it two cycles of latency).

Possible solution: concede flop-based TLBS, keep SRAM-based cache. Address register of SRAM becomes output flop of F1/E1, output register of SRAM is output register of F2/E2.

# Top LETC Core Diagram

## v0.9.0

![letc_core_top-v0 9 0 drawio](https://github.com/user-attachments/assets/bf2f509e-6b7b-4887-beb8-645f1b6c2c1b)

## v0.7.0

![letc_core_top-v0 7 0 drawio](https://github.com/angry-goose-initiative/wiki/assets/40674968/1d0a5c44-ab5a-413a-8636-39a24d390142)

# Register File

![letc_core_rf drawio](https://github.com/angry-goose-initiative/wiki/assets/40674968/10645ca3-9462-46cc-9556-72977485a25a)

# AXI FSM

![letc_core_axi_fsm drawio](https://github.com/angry-goose-initiative/wiki/assets/40674968/429d4236-9ddd-46e0-a68d-a2c4d7bdfd8b)

# ALU Operand Muxing

- Ignoring rs1/rs2 bypassing from other stages, we need to support the following combinations of operands into the ALU:

| Operand 1 | Operand 2 | Operation | Needy Instructions | Notes |
|-|-|-|-|-|
|`rs1_rdata`|`rs2_rdata`|Any arithmetic one|All `OP` instructions||
|`rs1_rdata`|`immediate`|Any arithmetic one|All `OP_IMM`, `LOAD`, `STORE` instructions|Only `ALU_OP_ADD` used for memory instructions|
|Current PC|`immediate`|`ALU_OP_ADD`|`auipc`||
|Current PC|Constant 4|`ALU_OP_ADD`|`jal`, `jalr`||
|Constant 0/Don't Care|`immediate`|`ALU_OP_ADD`/`ALU_OP_PASS2`|`lui`, `csrrwi`|The ALU can either just add zero to the immediate or just pass through operand 2|
|Constant 0/Don't Care|`rs1_rdata`|`ALU_OP_ADD`/`ALU_OP_PASS2`|`csrrw`|The ALU can either just add zero to RS1 or just pass through operand 2|
|`csr_rdata`|`immediate`|`ALU_OP_OR`, `ALU_OP_MCLR`|`csrrsi`, `csrrci`|Or used for setting, mask clear for clearing. Mask clear is NOT symmetric, or is.|
|`csr_rdata`|`rs1_rdata`|`ALU_OP_OR`, `ALU_OP_MCLR`|`csrrs`, `csrrc`|Or used for setting, mask clear for clearing. Mask clear is NOT symmetric, or is.|

Decision:
- We will mux between `rs1_rdata`, the current PC, `csr_rdata`, and the constant 0 for OP1
- We will mux between `rs1_rdata`, `rs2_rdata`, the immediate, and the constant 4 for OP2
- Pass through will be done by adding constant 0 to op2

# Cache

todo commentary

![letc_core_cache-More well defined drawio](https://github.com/angry-goose-initiative/wiki/assets/40674968/50209e4b-4b70-431b-9557-c0c9013eef6e)

Cache FSM behaviour:

![Screenshot 2024-04-07 120622](https://github.com/angry-goose-initiative/wiki/assets/67602506/5ec5629f-8871-403b-bc26-89485674b5d7)


# Stages

## F1

todo commentary

![letc_core_stage_f1 drawio](https://github.com/angry-goose-initiative/wiki/assets/40674968/2b076051-2544-4d9a-82a5-9d6af13ffca4)

## F2

TODO

## D

TODO

## E1

todo commentary

![letc_core_stage_e1 drawio](https://github.com/angry-goose-initiative/wiki/assets/40674968/f4b4fa16-ae74-477a-bfc9-1d4174dbaa83)

## E2

TODO

## W

todo commentary

![letc_core_stage_w drawio](https://github.com/angry-goose-initiative/wiki/assets/40674968/720f0de6-666e-4860-8d49-8c38bcdbca33)
