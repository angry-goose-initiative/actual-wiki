Hi there! This page details what you need to do to quickly get a "hello world" program running on IRVE!


# Get all the things

We highly recommend doing all of this from a Linux system. We have had some success on macOS, but with the RISC-V toolchain in particular you might encounter issues.

Ensure you have a modern G++ compiler installed that supports C++23. (Clang++ works too with some CMake variable tweaks).

You'll also want a cross compiler if you actually want something to *run on the emulator* (though you don't need it for the emulator itself).
We use [riscv-gnu-toolchain](https://github.com/riscv-collab/riscv-gnu-toolchain) for this.
It's **imperative** that you build the Newlib version (since the RVSW test programs don't use dynamic linking as they are bare-metal).
Also, since IRVE implements RV32IMA (and some other smaller extensions), you **must** append `--with-arch=rv32ima` and `--with-abi=ilp32` to the `./configure` command.

Don't forget to add all of the `riscv32-unknown-elf-*` binaries to your PATH too!

[More details about compiling the bare-metal RISC-V toolchain are available here](https://github.com/angry-goose-initiative/wiki/wiki/Setting-up-the-RISC%E2%80%90V-Toolchain-(Bare-Metal)).

Lastly, in a directory of your choice, clone this repo:

```
$ git clone https://github.com/angry-goose-initiative/irve.git
Cloning into 'irve'...
remote: Enumerating objects: 4224, done.
remote: Counting objects: 100% (659/659), done.
remote: Compressing objects: 100% (277/277), done.
remote: Total 4224 (delta 416), reused 476 (delta 377), pack-reused 3565
Receiving objects: 100% (4224/4224), 931.98 KiB | 1.75 MiB/s, done.
Resolving deltas: 100% (2891/2891), done.
$
```

With that, you should be set to continue!

# Compile all the things

We use CMake as our primary build system to compile both `libirve`, our two emulator frontends `irve` and `irvegdb`, and all of our RISC-V test code!

Step 0: Make a build directory in the root of the project with `mkdir build`

Step 1: Go to the build directory with `cd build`

Step 2: Do `cmake ..` to setup CMake (or `cmake -DCMAKE_BUILD_TYPE=Release ..` for a **much faster** release build)

Step 3: Perform the compile with `make`. For speed, you should probably do `make -j <Number of threads here>` instead to use all of your host's threads

NOTE: If you just want to compile `irve` (ex. if you don't have a cross compiler), do `make irve` instead.

# Run all the things

The `irve` front end (the one you likely care about) is placed in `build/src/irve`, but the symlink in the root of the project already links to this for your convenience.

Every argument to `irve` is a binary file to load into memory. Files containing a `.vhex8` suffix are assumed to be 8-bit-wide Verilog hex files, and are loaded into memory starting at 0x00000000, unless the addresses in the file (@xxxxxxxx) specify otherwise. You can also load bare-metal `.elf` files, which get placed into memory at the proper locations. At the moment, every other file extension is assumed to be a flat binary and is loaded at 0x80000000. This offset is useful if you [want to run the Linux kernel](https://github.com/angry-goose-initiative/wiki/wiki/Linux-shenanigans)!

Assuming you've build all of the RVSW testfiles, you can run a test program like `hello_world` like this (**from the root of the repo**):

```
$ ./irve rvsw/compiled/src/single_file/c/hello_world.vhex8 
IRVE> Starting IRVE
IRVE>  ___ ______     _______  __
IRVE> |_ _|  _ \ \   / / ____| \ \
IRVE>  | || |_) \ \ / /|  _|    \ \
IRVE>  | ||  _ < \ V / | |___   / /
IRVE> |___|_| \_\ \_/  |_____| /_/
IRVE> 
IRVE> The Inextensible RISC-V Emulator
IRVE> Copyright (C) 2023-2024 John Jekel
IRVE> Copyright (C) 2023-2024 Nick Chan
IRVE> See the LICENSE file at the root of the project for licensing info.
IRVE> 
IRVE> libirve Version 0.8.0 Commit 6371bf50e0bf3525ebee081a3c7fa3a31ffbf58e
IRVE> libirve built at 23:21:03 on Jan 18 2024
IRVE> irve executable built at 23:21:10 on Jan 18 2024
IRVE> ------------------------------------------------------------------------
IRVE> 
IRVE> 
IRVE> Fuzzish Build: Set seed to 1690508132
IRVE> Initializing emulator...
IRVE> Loading memory image from file "rvsw/compiled/src/single_file/c/hello_world.vhex8"
IRVE> Initialized the emulator in 1489348us
IRVE> RV: "Hello World!\n"
IRVE> Emulation finished in 1499us
IRVE> 1574 instructions were executed
IRVE> Average of 1050033.355570 instructions per second (1.050033MHz)
IRVE> IRVE is shutting down. Bye bye!
$
```

Or, to run an S-mode test program, specify the OGSBI binary first:

```
$ ./irve rvsw/compiled/sbi/ogsbi/ogsbi.vhex8 rvsw/compiled/src/single_file/cxx/hello_cxx_smode.vhex8 
IRVE> Starting IRVE
[...]
IRVE> Initializing emulator...
IRVE> Loading memory image from file "rvsw/compiled/sbi/ogsbi/ogsbi.vhex8"
IRVE> Loading memory image from file "rvsw/compiled/src/single_file/cxx/hello_cxx_smode.vhex8"
IRVE> Initialized the emulator in 1526844us
IRVE> RV: "OGSBI> OGSBI is starting up...\n"
IRVE> RV: "OGSBI>   ___   ____ ____  ____ ___\n"
IRVE> RV: "OGSBI>  / _ \ / ___/ ___|| __ )_ _|\n"
IRVE> RV: "OGSBI> | | | | |  _\___ \|  _ \| |\n"
IRVE> RV: "OGSBI> | |_| | |_| |___) | |_) | |\n"
IRVE> RV: "OGSBI>  \___/ \____|____/|____/___|\n"
IRVE> RV: "\n"
IRVE> RV: "OGSBI> RVSW's First SBI Implementation\n"
IRVE> RV: "OGSBI> Copyright (C) 2023 John Jekel and Nick Chan\n"
IRVE> RV: "OGSBI> See the LICENSE file at the root of the project for licensing info.\n"
[...]
IRVE> RV: "OGSBI> Function: sbi_debug_console_write()\n"
IRVE> RV: "Hello World from hello_world_t::hello_world_t()!\n
[...]
IRVE> RV: "OGSBI> LEGACY Function: sbi_shutdown()\n"
IRVE> RV: "OGSBI>   Shutting down, au revoir! ...\n"
IRVE> Emulation finished in 34219us
IRVE> 55761 instructions were executed
IRVE> Average of 1629533.300213 instructions per second (1.629533MHz)
IRVE> IRVE is shutting down. Bye bye!
$
```

# Run the Linux kernel

Want to run something more interesting?

See the [Linux shenanigans](https://github.com/angry-goose-initiative/wiki/wiki/Linux-shenanigans) page.

