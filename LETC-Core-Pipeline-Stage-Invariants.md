1. If not reset:
    - Input valid is known
    - Stall signal is known
    - Flush signal is known
    - Flush and stall are not asserted at the same time
2. Outputs remain constant when stage is stalled
3. When stalled, the input and output valid are low

## Policy with ready, stalling, flushing, valids, etc

Other than producing the signal for consumption by adhesive, stages should not touch their own ready signal AT ALL.

Instead, in order to stall, it's expected that adhesive would "loop back" the ready, inverted as the stall signal to the stage (as well as propagate it backwards).

Both flushing and stalling gate the valid going OUT of a stage (they are inverted and and-ed with the valid flop at the start of the stage). The difference is that stalling also stops the input flops of the stage from latching new data, whereas flushing doesn't, thereby destroying it (as it is replaced by the inputs from the previous stage and isn't preserved as it goes to the next stage).