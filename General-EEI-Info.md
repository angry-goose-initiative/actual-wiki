To run the Linux kernel, we must implement the base integer ISA (RV32I) as well as the M, A, Zifencei, and Zicsr extensions.

We will also be using SV32 which is the only paging option for a 32-bit system.

We actually do implement all of these in "hardware" in IRVE, however in LETC we plan to take some shortcuts and emulate some things in OGSBI.

This OGSBI emulation however will be invisible to all S-Mode software. Things we'll emulate include the M extension (at least division instructions), at least a few CSRs, and potentially some subset of the A extension. We intend to implement hardware page table walkers however for example.


Since we only have a single hart, we support both RVWMO and RVTSO binaries.

Details about CSRs are on [this page](https://github.com/angry-goose-initiative/wiki/wiki/Control-Status-Registers-(CSRs))