# IRVE

TODO

# LETC

TODO improve this

Path | Description
-----|-------------
/external_ip | IP and IP configuration files for external vendors used in LETC
/ide | IDE-specific files (ex. Vivado, Quartus)
/rtl | SystemVerilog RTL files (synthesizable)
/rtl/common | Generally useful IP (not LETC specific, but used in LETC)
/rtl/letc | LETC-specific RTL
/synth | Files to synthesize LETC
/synth/fc | Files to synthesize LETC in its entirety
/synth/subblock | Files to synthesize individual LETC sub-blocks and IP (out of context synthesis)
/verif | Files for verifying RTL (non-synthesizable)
/verif/lib | Reusable SystemVerilog files in testbenches / for UVM purposes
/verif/nonuvm | Designer (non-uvm) testbenches (for immediate testing and debugging purposes)
/verif/uvm | Proper UVM testing framework