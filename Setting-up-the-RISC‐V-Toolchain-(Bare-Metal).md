1. Clone the [RISC-V GNU Compiler Toolchain](https://github.com/riscv/riscv-gnu-toolchain)
```
git clone https://github.com/riscv/riscv-gnu-toolchain
```

2. Install the dependencies for your system specified in the toolchain's readme

3. Run this command at the root of the toolchain:
```
./configure --prefix=/opt/riscv --with-arch=rv32ima --with-abi=ilp32
```
The first option sets the location that the toolchain is stored to `opt/riscv` and the other two options are so that the toolchain targets the correct RISC-V implementation.

Note: If you run into errors building this on MacOS, try adding `--disable-gdb` to the command above which disables GDB.

4. Run `make -j` at the root of the toolchain (or likely it will need to be `sudo make -j` to have proper permissions). This takes a while.

5. Add `opt/riscv/bin` to your PATH.

Note: Build the newlib version since rvsw test programs don't use dynamic linking since they're bare metal