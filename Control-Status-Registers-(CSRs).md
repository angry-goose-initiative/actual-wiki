_This page documents the behavior of all the CSRs in our implementation_

TODO add wavedrom register diagrams for this

### sstatus
0x100
- Supervisor status register
- Restricted view of [mstatus](https://github.com/angry-goose-initiative/wiki/wiki/Control-Status-Registers-(CSRs)#mstatus)
- mstatus[23:20, 17, 12:11, 7, 3] are restricted (read-only zero)

### sie
0x104
- Supervisor interrupt-enable register
- Restricted view of [mie](https://github.com/angry-goose-initiative/wiki/wiki/Control-Status-Registers-(CSRs)#mie)

### stvec
0x105
- Supervisor trap handler bass address
- Holds trap vector configuration
- stvec[31:2]: BASE (Trap vector base address)
- stvec[1:0]: MODE
- All synchronous exceptions into S-mode will cause the pc to be set to the the address in the BASE field, whereas interrupts cause the pc to be set to the address in the BASE field plus four times the interrupt cause number (scause shifted left by 2).

### scounteren
0x106
- Supervisor counter enable
- We never allow U-mode to read performance counters so this will be read-only zero

### senvcfg
0x10A
- Supervisor environment configuration register
- senvcfg[31:8]: WPRI (read-only 0)
- senvcfg[7:4]: Allocated for future extensions, we will make these read-only zero
- senvcfg[3:1]: WPRI (read-only 0)
- senvcfg[0]: FIOM

### sscratch
0x140
- Scratch register for supervisor trap handlers
- Holds software-controlled data so R/W
- Typically, sscratch is used to hold a pointer to the hart-local supervisor context while the hart is executing user code. At the beginning of a trap handler, sscratch is swapped with a user register to provide an initial working register

### sepc
0x141
- Supervisor exception program counter
- sepc[1:0]: read-only zero since our IALIGN=32
- When a trap is taken into S-mode (from S or U-mode), sepc is written with the virtual address of the instruction that was interrupted or that encountered the exception
- Can also be written by software

### scause
0x142
- Supervisor (trap) cause register
- When a trap is taken into S-mode (from S-mode or U-mode), scause is written with a code indicating which event that caused the trap
- Can also be written by software
- scause\[31]: Interrupt bit, set if the trap was caused by interrupt
- scause[31:0]: Exception code
    - This field is required to hold values 0-31 so scause[4:0] must be writable but the other bits don’t have to be thus scause[31:5] is read-only zero

### stval
0x143
- Supervisor bad address or instruction
- When a trap is taken into S-mode (from S-mode or U-mode), stval is written with exception-specific information to assist software in handling the trap 
    - Seems like it’s mainly for storing faulting virtual addresses
- For traps where this isn’t explicitly set to a value, it will be set to 0. In our case, all traps taken into S-mode will set this reg to 0.
- Can also be written by software

### sip
0x144
- Supervisor Interrupt Pending
- Contains information on pending interrupts
- Interrupt cause number _i_ corresponds with bit _i_ in sip
- sip[31:16]: Custom/platform use, we will make these read-only 0
- sip[15:0]: Standard interrupt causes

### satp
0x180
- Supervisor address translation and protection
- satp\[31]: MODE
    - Selects the current address-translation scheme
    - 0: bare (no translation or protection)
    - 1: Sv32 (Page-based 32-bit virtual addressing)
- satp[30:22]: ASID
    - Address space identifier
    - We don't support ASID's so this is read-only zero
- satp[21:0]: PPN
    - Physical page number of the root page table, i.e. its supervisor physical address divided by 4 KiB

### mstatus
0x300
- Machine status register (low)
- mstatus[31]: SD
    - Read-only and set if FS, VS, or XS is set to 11. Since those are read-only zero, this is read-only zero
- mstatus[30:23]: WPRI (read-only zero)
- mstatus[22]: TSR
    - Trap SRET
    - When this field is set, attempts to execute SRET while operating in S-mode will raise an illegal instruction exception
- mstatus[21]: TW
    - Timeout wait
    - A bit that should be writable, but won’t have any effect since we can make the timeout wait 0 which would make an attempt to execute a WFI instruction raise an illegal instruciton exception whether this is set or not
    - For simplification, we will make this read-only zero
- mstatus[20]: TVM
    - Trap Virtual Memory
    - When TVM is set, attempts to read or write to satp or execute an SFENCE.VMA or SINVAL.VMA instruction while executing in S-mode will raise an illegal instruction exception
    - When TVM=0, these operations are permitted in S-mode
- mstatus[19]: MXR
    - Make eXecutable Readable (modifies the privilege with which loads access virtual memory)
    - When MXR=0, only loads from pages marked readable will succeed
    - When MXR=1, loads from pages marked either readable or executable (R=1 or X=1) will succeed
    - MXR has no effect when page-based virtual memory is not in effect
- mstatus[18]: SUM
    - permit Superisor User Memory access (modifies the privilege with which S-mode loads and stores access virtual memory)
    - When SUM = 0, memory accesses to pages that are accessible by U-mode (pages with bit U set) will fault when either
        - In S-mode or
        - MPRV = 1, MPP = S, and the access is either a load or a store
- mstatus[17]: MPRV
    - Modify PriVilege (modifies effective privilege mode), or the privilege mode at which loads and stores execute
    - When MPRV is set, address translation and protection for loads and stores behaves as if the current privilege mode was set to MPP
    - NOTE: this does not have an effect on instruction address protection and translation
    - An MRET or SRET instruction that changes the privilege mode to a mode less privileged than M also sets MPRV=0
- mstatus[16:15]: XS
    - Since we aren’t implementing additional user extensions, this will be read only 0
- mstatus[14:13]: FS
    - Since we aren’t implementing the F extension (or emulating it), this will be read only 0
- mstatus[12:11]: MPP
    - Holds previous privilege mode prior to trap
    - When a trap is taken into M-mode, MPP is set with the active privilege mode at the time of the trap
    - When executing a MRET instruction, this field is set to 0b00
- mstatus[10:9]: VS
    - Since we aren’t implementing the V extension (or emulating it), this will be read only 0
- mstatus[8]: SPP
    - Holds previous privilege mode prior to trap
    - When a trap is taken into to S-mode, SPP is set with the active privilege mode at the time of the trap
    - When executing a SRET instruction, this field is set to 0
    - Note that this is only one bit because traps to S-mode can only come from U-mode (0) or S-mode (1)
- mstatus[7]: MPIE
    - Indicates whether machine interrupts were enabled prior to trapping into M-mode
    - When a MRET instruction is executed, this bit is set to 1
    - When a trap is taken into M-mode, this is written with the value of MIE at the time of the trap
- mstatus[6]: UBE
    - Controls endianness of memory accesses other than instruction fetches
    - Since we are only supporting little-endian, this will be read-only 0
- mstatus[5]: SPIE
    - Indicates whether supervisor interrupts were enabled prior to trapping into S-mode
    - When a SRET instruction is executed, this bit is set to 1
    - When a trap is taken into S-mode, this is written with the value of SIE at the time of the trap
- mstatus[4]: WPRI (read-only 0)
- mstatus[3]: MIE
    - Enables/disables all interrupts in M-mode
    - When this bit is clear, interrupts are not taken while in M-mode
    - When in S or U-mode, the value in MIE is ignored and machine level interrupts are enabled
    - When a trap is taken into M-mode, this bit is set to 0
    - When a MRET instruction is executed, this is set to the value of MPIE
- mstatus[2]: WPRI (read-only 0)
- mstatus[1]: SIE
    - Enables/disables all interrupts in S-mode
    - When this bit is clear, interrupts are not taken while in S-mode
    - When in U-mode, the value in SIE is ignored and supervisor level interrupts are enabled
    - The supervisor can disable individual interrupt sources using the sie CSR
    - When a trap is taken into S-mode, this bit is set to 0
    - When a SRET instruction is executed, this is set to the value of SPIE
- mstatus[0]: WPRI (read-only 0)

### misa
0x301
- Machine ISA register
- We don't implement this register so it's read-only zero

### medeleg
0x302
- Machine exception delegation register
- Setting a bit will delegate the corresponding trap, when occurring in S-mode or U-mode, to the S-mode trap handler
- Used to indicate that certain interrupts and exceptions should be handled by a lower privilege level
- This register has a bit position allocated for every synchronous exception (table on page 39)

### mideleg
0x303
- Machine interrupt delegation register
- Setting a bit will delegate the corresponding trap, when occurring in S-mode or U-mode, to the S-mode trap handler

### mie
0x304
- Machine Interrupt-Enable Register
- Interrupt cause number *i* corresponds with bit *i*
- Interrupt *i* will trap to M-mode if all are true:
    - Either the current privilege mode is M and mstatus.MIE is set, or the current privilege mode is less than M
    - Bit *i* is set in both mip and mie
    - Bit *i* is not set in mideleg

Interrupt cause number _i_ corresponds to bit _i_ in sie
- sie[31:16] is for custom/platform use, we will make these read-only 0
- sie[15:0]: standard interrupt cause
- An interrupt _i_ will trap to S-mode if both are true:
    - Either the current privilege mode is S and the SIE bit in sstatus (mstatus) is set or the current privilege mode is U and
    - Bit _i_ is set in both sip and sie

- mie[31:16] are for platform or custom use. We will likely make these read-only zero.
- mie[15:0] are for standard cause interrputs
    - mie[15:12, all other even bits]: read-only 0
    - mie[11]: MEIP, interrupt enable bit for machine level external interrupts
    - mie[9]: SEIP, interrupt enable bit for supervisor level external interrupts (more on page 33)
    - mie[7]: MTIP, interrupt enable bit for machine timer interrupts
    - mie[5]: STIE, interrupt enable bit for supervisor timer interrupts 
    - mie[3]: MSIP, interrupt enable bit for machine level software interrupts
    - mie[1]: SSIP, interrupt enable bit for supervisor level software interrupts (more on page 33)
- A bit in mie must be writable if the corresponding interrupt can ever become pending. Non writable bits must be read-only 0
See page 32
- Restricted view of mie register appears as sie for supervisor level (page 34)

### mtvec
0x305
- Machine trap-handler base address
- Holds trap-vector configuration
- mtvec[31:2]: BASE, Trap vector base address
    - The base address (in decimal) is 4
- mtvec[1:0]: MODE
    - This field will be read-only 0b01 (vectored)
    - All synchronous exceptions into machine mode will cause the pc to be set to the the address in the BASE field, whereas interrupts cause the pc to be set to the address in the BASE field plus four times the interrupt cause number (mcause shifted left by 2).
- This register will be read-only 0x00000011

### mcounteren
0x306
- Counter-enable register
- Controls the availability of hardware performance-monitoring counters to the next-lowest privileged mode (S-mode in our case).
- mcounteren[31:3]: HPMn (n is bit number). Since we aren’t using the extra counters, these can be read-only 0.
- mcounteren[2]: IR
- mcounteren[1]: TM
- mcounteren[0]: CY
- When the CY, TM, IR, or HPMn bit in the mcounteren register is clear, attempts to read the cycle, time, instret, or hpmcountern register while executing in S-mode or U-mode will cause an illegal instruction exception. When one of these bits is set, access to the corresponding register is permitted in S-mode.
- We are making this register read-only zero

### menvcfg
0x30A
- Machine Environment Configuration Register (Low)
- Controls certain settings for privilege levels lower than Machine
- menvcfg[31:8]: WPRI
- menvcfg[7]: CBZE
    - Used by a future extension that does not exist.
    - No idea what this should be set to. 0 is likely safe
- menvcfg[6]: CBCFE
    - Used by future extension that doesn’t exist
    - No idea what this should be set to. 0 is likely safe
- menvcfg[5:4]: CBIE
    - Used by a future extension that does not exist.
    - No idea what this should be set to. 0 is likely safe
- menvcfg[1]: WPRI
- menvcfg[0]: FIOM
    - Bit that doesn't not matter for our purposes; must be R/W
- We will make all bits except for [0] read-only 0

### mstatush
0x310
- Machine status register (high)
- Read-only zero

### menvcfgh
0x31A
- Machine Environment Configuration Register (High)
- Controls certain settings for privilege levels lower than Machine
- menvcfgh[31]: STCE
    - Used by a future extension that does not exist.
    - No idea what this should be set to. 0 is likely safe
- menvcfgh[30]: PBMTE
    - Use by the Svpbmt, but we’re not implementing that extension.
    - Thus this must be read-only 0
- menvcfgh[29:0]: WPRI (read-only 0)
- This register will be read-only 0

### mcountinhibit
0x320
- Machine counter-inhibit register
- Controls which of the hardware performance-monitering counters increment
- mcountinhibit[31:3]: HPM\<n> (**n** is bit number). Since we aren’t using the extra counters, these can be read-only 0.
- mcountinhibit[2]: IR
- mcountinhibit[1] Read-only 0
- mcountinhibit[0]: CY
- From page 37, we might be able to not implement this and make it read-only 0 (we are doing this for now).

### mhpmevent\<n>
0x323 - 0x33F
- Machine performance-monitoring event selector
- n ∈ [3,31]
- Since we aren’t using the extra counters, these can be read-only 0

### mscratch
0x340
- Scratch register for machine trap handlers
- Holds software-controlled data, must be readable and writable
- Typically, it is used to hold a pointer to a machine-mode hart-local context space and swapped with a user register upon entry to an M-mode trap handler.

### mepc
0x341
- Machine exception program counter
- mepc[1:0] are read-only zero
- When a trap is taken into M-mode, mepc is written with the virtual address of the instruction that was interrupted or encountered an exception
- Can also be written by software

### mcause
0x342
- Machine trap cause
- When a trap is taken into M-mode, mcause is written with a code indicating the event that caused the trap
- Can also be written by software
- mcause[31]: Interrupt bit, set if the trap was caused by an interrupt
- mcause[31:0]: Exception code (see table 3.6 on page 39 for the exception codes)

### mtval
0x343
- Machine bad address or instruction
- Read-only zero

### mip
0x344
- Machine Interrupt-Pending
- This and mie are very similar
- Cause number *i* corresponds with bit *i*
- An interrupt *i* will trap to M-mode if all are true:
    - Either the current privilege mode is M and the MIE bit in the mstatus register is set, or the current privilege mode is less than M-mode
    - Bit *i* is set in both mip and mie
    - Bit *i* is not set in mideleg
- mip[31:16] are for platform or custom use. We will likely make these read-only 0.
- mip[15:0] are for standard cause interrupts
    - mip[15:12, all other even bits]: read-only 0
    - mip[11]: MEIP, interrupt pending bit for machine level external interrupts
    - mip[9]: SEIP, interrupt pending bit for supervisor level external interrupts (more on page 33)
    - mip[7]: MTIP, interrupt pending bit for machine timer interrupts
    - mip[5]: STIE, interrupt pending bit for supervisor timer interrupts 
    - mip[3]: MSIP, interrupt pending bit for machine level software interrupts
    - mip[1]: SSIP, interrupt pending bit for supervisor level software interrupts (more on page 33)
- Each bit can be writable or read-only. When bit *i* in mip is writable, a pending interrupt *i* can be cleared by writing 0 to this bit. If interrupt *i* can become pending but bit i in mip is read-only, the implementation must provide some other mechanism for clearing the pending interrupt.
- See page 32
- Restricted view of mip register appears as sip for supervisor level (page 34)

### mtinst
0x34A
- Machine trap instruction (transformed)
- We will likely use this for emulating instructions
- See 8.6.3 (page 131)

### pmpcfg\<n>
0x3A\<0,2,4,6,...,62>
- Physical memory protection configuration (low)
- WARL and since we don't support PMP these are all read-only zero

### pmpcfgh\<n>
0x3A\<1,3,5,7,...,63>
- Physical memory protection configuration (high)
- WARL and since we don't support PMP these are all read-only zero

### mcycle
0xB00
- Number of cycles since arbitrary time in the past (low)

### minstret
0xB02
- Machine instructions-retired counter (low)
- Number of instructions retired by hart since arbitrary time in the past

### mhpmcounter\<n>
0xB03 - 0xB1F
- Performance monitoring counters (low)
- n ∈ [3,31]
- We aren’t using these so they will be read-only zero

### mcycleh
0xB80
- Number of cycles since arbitrary time in the past

### minstreth
0xB82
- Machine instructions-retired counter (high)
- Number of instructions retired by hart since arbitrary time in the past (Machine)

### mhpmcounter\<n>h
0xB83 - 0xB9F
- Performance monitoring counters (high) (Machine)
- n ∈ [3,31]
- We aren’t using these so they will be read-only 0 (cannot throw exception)

### cycle
0xC00
- Cycle counter for RDCYCLE instruction (low)
- Number of cycles since arbitrary time in the past (User)

### time
0xC01
- Shadow of the memory-mapped mtime register (low) (User)
- Read-only

### instret
0xC02
- Number of instructions retired by hart since arbitrary time in the past (low) (User)

### hpmcounter\<n>
0xC03 - 0xC1F
- Performance monitoring counters (User)
- n ∈ [3,31]
- These registers must be implemented, but we aren’t using any additional counters so we will have them be read-only zero

### cycleh
0xC80
- Cycle counter for RDCYCLE instruction (high)
- Number of cycles since arbitrary time in the past (User)

### timeh
0xC81
- Shadow of the memory-mapped mtime register (high) (User)
- Read-only

### instreth
0xC82
- Number of instructions retired by hart since arbitrary time in the past (high) (User)

### hpmcounter\<n>h
0xC83 - 0xC9F
- Performance monitoring counters (User)
- n ∈ [3,31]
- These registers must be implemented, but we aren’t using any additional counters so we will have them be read-only zero

### mvendorid
0xF11
- Vendor ID
- In our case this will be read-only 0 to indicate that this is a non-commercial implementation

### marchid
0xF12
- Architecture ID
- This will be read-only 0 to indicate that this field in not implemented
- RISC-V International allocates open-source project architecture IDs, so maybe we can look into that in the (likely far) future.

### mimpid
0xF12
- Implementation ID
- This will be read-only 0 to indicate that this field is not implemented

### mhartid
0xF14
- Hardware thread ID
- One hart must have an ID of 0, and since we will only have one hart, this will be read-only zero

### mconfigptr
0xF15
- Pointer to configuration data structure










