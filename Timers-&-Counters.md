_This page contains notes about timers and counters_

- Critically, TIMERS $\neq$ COUNTERS !!!
- There are several counters:
    - mcycle (a CSR) - Cycle counter
    - minstret (also a CSR) - Instructions retired counter
    - mhpmcounterX (all CSRs) - Implementation-specific counters
    - mtime (memory mapped) - Real time counter
- THERE IS ONLY ONE “NATIVE” COUNTER THAT CAN CAUSE TIMER INTERRUPTS
    - That is mtime with the associated mtimecmp
    - An interrupt is only generated if MTIE in mie is set, and of course interrupts are globally enabled
    - Of course, an external timer peripheral could be hooked up to a RISC-V PMIC/other IC, but then it would appear as an external interrupt, not a timer one
- THERE ARE NO HARDWARE SUPERVISOR TIMER INTERRUPTS, PERIOD
    - M-Mode can “multiplex”/emulate any number of desired timers for S-Mode by setting the timer interrupt pending bit when supervisor mode should be woken up
