# AXI

AXI is the main bus protocol used in LETC. The PS speaks AXI, and so too must we.

AXI is a very high-performance protocol, featuring:

- Burst transfers: Instead of transferring data one element at a time, with AXI you can send large chunks of data together.
- Out-of-order transactions: Managers (like LETC Core) can issue multiple read/write requests without waiting for each to complete, enabling greater parallelism.
- Multiple outstanding transactions: Managers can have multiple transactions "in flight" at the same time, minimizing idle periods.
- Reads during writes: The master can initiate read requests while write transactions are still ongoing and vice-versa.

While we may not take advantage of all of these features in LETC, at least not right away, keeping them in mind will allow us to upgrade LETC's capabilities in the future. At the very minimum we want to keep LETC IP compatible so as to be able to reuse parts in later projects.

The best resource for AXI for us is [ARM IHI 0022 H.C](https://developer.arm.com/documentation/ihi0022/hc/) (NOT later versions) as it covers AXI3 and AXI4, both of which are of concern to us. While we intend to target AXI4 for everything in soft-logic, the PS uses AXI3 so we will need to bridge between the two in LETC Matrix (or outside perhaps).

# LIMP

LIMP, the LETC Internal Memory Protocol, was our originally planned bus protocol for the project. However, wanting to allow for better re-usability in future projects, and to interface with the PS of the Zync 7000 architecture, switching to AXI was a natural decision.

We may reuse parts of LIMP for arbitration within LETC core, as several sources including the TLBs/MMU, and caches will be fighting for a turn with Core's single AXI port.

LIMP uses a very simple data-valid-ready scheme and can only work on one transfer at a time. It has control lines to indicate if a transaction is a read or a write, and also the size of the data to be read or written.

todo describe more details, waveform diagrams, etc

