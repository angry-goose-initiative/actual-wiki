*Ah, yes, the wonderful world of exceptions and interrupts. Here are the steps the CPU follows (roughly)!*

# Exceptions
| Step Number | M Mode Operations | S Mode Operations|
|-|-|-|
|1|The exception occurs|The exception occurs|
|2||medeleg is implicitly read. If the bit corresponding to this exception is set, we handle the exception in S mode (go down); otherwise go left and handle this in m mode|
|3|Begin to implicitly write mstatus bits to manipulate the privilege stack: MPIE gets the value in MIE (“previous interrupt enable value”) |Begin to implicitly write sstatus bits to manipulate the privilege stack: SPIE gets the value in SIE (“previous interrupt enable value”)|
|4|MIE is then made 0 (to disable interrupts to avoid clobbering state until the trap finishes)|SIE is then made 0 (to disable interrupts to avoid clobbering state until the trap finishes)|
|5|MPP is written with the privilege level of where the exception occured|SPP is written with the privilege level of where the exception occured (S mode or U mode)|
|6|Write mcause with the cause|Write scause with the cause|
|7|Optionally write mtval|Optionally write stval|
|8|Write mepc with the (virtual) address that caused the trap|Write sepc with the (virtual) address that caused the trap|
|9|Read mtvec and set the PC to the relevant address|Read stvec and set the PC to the relevant address|
|10|MRET|SRET|
|11|Update mstatus again: MIE gets the value of MPIE|Update sstatus again: SIE gets the value of SPIE|
|12|The privilege mode goes back to the value in MPP|The privilege mode goes back to the value in SPP|
|13|MPP is set to the least-privilidged mode the hardware supports (helps to identify software bugs, not of significance in and of itself)|SPP is set to the least-privilidged mode the hardware supports (helps to identify software bugs, not of significance in and of itself)|
|14|If the MRET was back to S mode or U mode (not M), MPRV becomes 0|MPRV becomes 0|
|15|Set the PC to the value in the mepc register|Set the PC to the value in the sepc register|

# Interrupts
todo